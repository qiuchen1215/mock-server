package com.yuanniao.qa.mock.service.core.spi.parser;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yuanniao.qa.mock.service.core.Context;
import com.yuanniao.qa.mock.service.core.kit.ErrorMsg;
import com.yuanniao.qa.mock.service.core.spi.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URLDecoder;

public class JsonParser implements Parser {
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonParser.class);
    private String enc = "UTF-8";

    @Override
    public boolean isMatch(String name) {
        return "json".equals(name);
    }

    @Override
    public void parse(Context context) {
        try {
            String script = context.getRuleScript();
            script = URLDecoder.decode(script, enc);
            JSONObject json = JSON.parseObject(script);
            if (json.containsKey("code")) {
                context.setCode(json.getIntValue("code"));
            }
            if (json.containsKey("body")) {
                context.setBody(json.getString("body").getBytes());
            }
            if (json.containsKey("header")) {
                JSONObject jo = json.getJSONObject("header");
                String[] header = new String[jo.keySet().size() * 2];
                int i = 0;
                for (String key : jo.keySet()) {
                    String value = jo.getString(key);
                    header[i] = key;
                    i++;
                    header[i] = value;
                }
                context.setHeader(header);
            }
            if (json.containsKey("func")) {
                JSONArray ja = json.getJSONArray("func");
                for (int i = 0, size = ja.size(); i < size; i++) {
                    String s = ja.getString(i);
                    String[] cmd = s.split("#");
                    if(cmd.length >= 1) {
                        if("sleep".equals(cmd[0]) && cmd.length == 2){
                            long ms = Long.valueOf(cmd[1]);
                            LOGGER.info(String.format("sleep start %s", context.getPath()));
                            Thread.sleep(ms);
                            LOGGER.info(String.format("sleep end %s", context.getPath()));
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw ErrorMsg.PARSER_FORMAT_ERROR;
        }
    }
}
