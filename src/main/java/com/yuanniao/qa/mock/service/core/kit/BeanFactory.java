package com.yuanniao.qa.mock.service.core.kit;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class BeanFactory {
    private static final String SET_METHOD = "set";
    private static final String INIT_METHOD = "init";
    private static final String WRAPPER_KEY_SUFFIX = "^";
    private static final Object NULL = new Object();

    public static <T> T createBean(Class<T> beanClass, Properties properties) {
        Map<String, Object> caches = new HashMap<String, Object>();
        String name = beanClass.getSimpleName();
        String property = StringKit.firstLetterToLowercase(name);
        String key = StringKit.splitCamelName(property, ".");
        String value = properties.getProperty(key);
        return getInstance(key, value, beanClass, properties, caches);
    }

    private static <T> T getInstance(String key, String value, Class<T> beanClass, Properties properties, Map<String, Object> caches) {
        if (StringKit.isEmpty(value) || "null".equals(value)) {
            return null;
        }
        Class<?> cls = forName(value);
        if (!beanClass.isAssignableFrom(cls)) {
            throw new IllegalStateException("The class " + cls.getName() + " unimplemented interface " + beanClass.getName() + " in config " + key);
        }
        try {
            String index = key + "=" + value;
            Object instance = caches.get(index);
            if (instance == null) {
                instance = cls.getConstructor(new Class<?>[0]).newInstance();
                caches.put(index, instance);
                injectInstance(instance, properties, key, caches);
                if (cls.getInterfaces().length > 0) {
                    Class<?> face = cls.getInterfaces()[0];
                    String insert = properties.getProperty(key + WRAPPER_KEY_SUFFIX);
                    if(StringKit.isNotEmpty(insert)) {
                        String[] wrappers = StringKit.splitByComma(insert);
                        for (String wrapper : wrappers) {
                            Class<?> wrapperClass = forName(wrapper);
                            if (!face.isAssignableFrom(wrapperClass)) {
                                throw new IllegalStateException("The wrapper class " + wrapperClass.getName() + " must be implements interface " + face.getName() + ", config key: " + key + WRAPPER_KEY_SUFFIX);
                            }
                            Constructor<?> constructor = wrapperClass.getConstructor(new Class<?>[] { face });
                            if (Modifier.isPublic(constructor.getModifiers())) {
                                Object wrapperInstance = constructor.newInstance(new Object[] {instance});
                                injectInstance(wrapperInstance, properties, key, caches);
                                instance = wrapperInstance;
                            }
                        }
                    }
                }
            }
            if (instance == NULL) {
                return null;
            }
            return (T) instance;
        } catch (Exception e) {
            throw new IllegalStateException("Failed to init property value. key: " + key + ", value: " + value + ", cause: " + e.getMessage(), e);
        }
    }

    private static void injectInstance(Object instance, Properties properties, String parent, Map<String, Object> caches) {
        try {
            Method[] methods = instance.getClass().getMethods();
            for (Method method : methods) {
                String name = method.getName();
                if (name.length() > 3 && name.startsWith(SET_METHOD)
                        && Modifier.isPublic(method.getModifiers())
                        && !Modifier.isStatic(method.getModifiers())
                        && method.getParameterTypes().length == 1) {
                    Class<?> parameterType = method.getParameterTypes()[0];
                    String property = name.substring(3, 4).toLowerCase() + name.substring(4);
                    String key = StringKit.splitCamelName(property, ".");
                    String value = null;
                    if (StringKit.isNotEmpty(parent)) {
                        value = properties.getProperty(parent + "." + key);
                    }
                    if (StringKit.isEmpty(value)) {
                        value = properties.getProperty(key);
                    }
                    Object obj = null;
                    if(StringKit.isNotEmpty(value)) {
                        if (parameterType.isArray()) {
                            Class<?> componentType = parameterType.getComponentType();
                            String[] values = StringKit.splitByComma(value);
                            Object objs = Array.newInstance(componentType, values.length);
                            for (int i = 0; i < values.length; i++) {
                                Object o = parseValue(key, values[i], componentType, properties, caches);
                                Array.set(objs, i, o);
                            }
                            obj = objs;
                        } else {
                            obj = parseValue(key, value, parameterType, properties, caches);
                        }
                    }
                    if (obj != null) {
                        method.invoke(instance, new Object[]{obj});
                    }
                }
            }
            try {
                Method method = instance.getClass().getMethod(INIT_METHOD, new Class<?>[0]);
                if (Modifier.isPublic(method.getModifiers())
                        && ! Modifier.isStatic(method.getModifiers())) {
                    method.invoke(instance, new Object[0]);
                }
            } catch (NoSuchMethodException e) {}
        } catch (Exception e) {
            throw new IllegalStateException("Failed to init properties of bean class " + instance.getClass().getName() + ", cause: " + e.getMessage(), e);
        }
    }

    private static Object parseValue(String key, String value, Class<?> parameterType, Properties properties, Map<String, Object> caches) {
        if (parameterType == String.class) {
            return value;
        } else if (parameterType == char.class) {
            return value.charAt(0);
        } else if (parameterType == int.class) {
            return Integer.valueOf(value);
        } else if (parameterType == long.class) {
            return Long.valueOf(value);
        } else if (parameterType == float.class) {
            return Float.valueOf(value);
        } else if (parameterType == double.class) {
            return Double.valueOf(value);
        } else if (parameterType == short.class) {
            return Short.valueOf(value);
        } else if (parameterType == byte.class) {
            return Byte.valueOf(value);
        } else if (parameterType == boolean.class) {
            return Boolean.valueOf(value);
        } else if (parameterType == Class.class) {
            return forName(value);
        } else {
            return getInstance(key, value, parameterType, properties, caches);
        }
    }

    private static Class<?> forName(String className) {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }
}
