package com.yuanniao.qa.mock.service.core.spi.rulemanager;


import com.yuanniao.qa.mock.service.core.Context;
import com.yuanniao.qa.mock.service.core.kit.ErrorMsg;

public class BaseRuleManager {
    protected void handleError(Context context, Exception e) {
        e.printStackTrace();
        if(e instanceof ErrorMsg) {
            context.setBody(e.toString().getBytes());
        } else {
            context.setBody(new ErrorMsg(500, e.getMessage()).toString().getBytes());
        }
    }

    protected void common(Context context) {
        if(context.getCode() == 0) {
            context.setCode(200);
        }
        if(context.getHeader() == null) {
            context.setHeader(new String[]{"Content-Type", "application/json"});
        }
    }
}
