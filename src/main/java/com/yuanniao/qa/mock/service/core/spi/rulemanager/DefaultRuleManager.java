package com.yuanniao.qa.mock.service.core.spi.rulemanager;


import com.yuanniao.qa.mock.service.core.Context;
import com.yuanniao.qa.mock.service.core.Rule;
import com.yuanniao.qa.mock.service.core.RuleManager;
import com.yuanniao.qa.mock.service.core.kit.ErrorMsg;
import com.yuanniao.qa.mock.service.core.kit.StringKit;
import com.yuanniao.qa.mock.service.core.spi.Parser;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class DefaultRuleManager extends BaseRuleManager implements RuleManager {
    private List<Rule> ruleList = new LinkedList<Rule>();
    private Parser[] parserList = new Parser[0];

    public void setParserList(Parser[] parserList) {
        this.parserList = parserList;
    }

    @Override
    public void dispatch(Context context) {
        String uri = context.getPath();
        try {
            if ("/internal/rule/add".equals(uri)) {
                addRule(context);
            } else if ("/internal/rule/remove".equals(uri)) {
                removeRule(context);
            } else if ("/internal/rule/query".equals(uri)) {
                queryRule(context);
            } else {
                findRuleAndParse(context);
            }
        } catch (Exception e) {
            handleError(context, e);
        } finally {
            common(context);
        }
    }

    private synchronized void addRule(Context context) {
        Rule rule = context.getRule();
        checkRule(rule);
        rule.setId(UUID.randomUUID().toString().replaceAll("-", ""));
        int index = findRuleIndex(rule);
        if (index >= 0) {
            ruleList.set(index, rule);
        } else {
            ruleList.add(rule);
        }
        context.setBody(String.format("{\"status\":0, \"id\":\"%s\"}", rule.getId()).getBytes());
    }

    private synchronized void removeRule(Context context) {
        Rule rule = context.getRule();
        String id = rule.getId();
        int index = findRuleIndexById(id);
        if (index >= 0) {
            ruleList.remove(index);
        }
        context.setBody(String.format("{\"status\":0}").getBytes());
    }

    private void queryRule(Context context) {
        Rule rule = context.getRule();
        int index = findRuleIndexById(rule.getId());
        if (index >= 0) {
            rule = ruleList.get(index);
            context.setBody(String.format("{\"status\":0, \"rule\":%s}", rule.toString()).getBytes());
        } else {
            throw ErrorMsg.RULE_NOT_FOUND;
        }
    }

    private void  findRuleAndParse(Context context) {
        int index = findRuleIndex(context);
        if(index >= 0) {
            context.getRule().setId(ruleList.get(index).getId());
            context.getRule().setParser(ruleList.get(index).getParser());
            context.getRule().setScript(ruleList.get(index).getScript());
            parse(context);
        } else {
            throw ErrorMsg.RULE_NOT_FOUND;
        }
    }

    private void parse(Context context) {
        boolean isParsed = false;
        for (int i = 0; i < parserList.length; i++) {
            if (parserList[i].isMatch(context.getRule().getParser())) {
                parserList[i].parse(context);
                isParsed = true;
            }
        }
        if (!isParsed) {
            throw ErrorMsg.PARSER_NOT_SUPPORTED;
        }
    }

    private int findRuleIndex(Rule rule) {
        int index = -1;
        int ruleListSize = ruleList.size();
        for (int i = 0; i < ruleListSize; i++) {
            if (rule.equals(ruleList.get(i))) {
                index = i;
                break;
            }
        }
        return index;
    }

    private int findRuleIndex(Context context) {
        int index = -1;
        int ruleListSize = ruleList.size();
        for (int i = 0; i < ruleListSize; i++) {
            Rule rule = ruleList.get(i);
            if (rule.getMethod().equals(context.getMethod()) &&
                    rule.getPath().equals(context.getPath()) &&
                    rule.getFrom().contains(context.getFrom())) {
                index = i;
                break;
            }
        }
        return index;
    }

    private int findRuleIndexById(String id) {
        int index = -1;
        int ruleListSize = ruleList.size();
        for (int i = 0; i < ruleListSize; i++) {
            if (ruleList.get(i).getId().equals(id)) {
                index = i;
                break;
            }
        }
        return index;
    }

    private void checkRule(Rule rule) {
        if (StringKit.isEmpty(rule.getMethod())) {
            throw ErrorMsg.RULE_METHOD_EMPTY;
        } else if (StringKit.isEmpty(rule.getPath())) {
            throw ErrorMsg.RULE_PATH_EMPTY;
        } else if (StringKit.isEmpty(rule.getScript())) {
            throw ErrorMsg.RULE_SCRIPT_EMPTY;
        }
    }
}
