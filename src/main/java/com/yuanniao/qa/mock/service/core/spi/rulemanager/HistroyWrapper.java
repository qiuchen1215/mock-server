package com.yuanniao.qa.mock.service.core.spi.rulemanager;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yuanniao.qa.mock.service.core.Context;
import com.yuanniao.qa.mock.service.core.RuleManager;
import com.yuanniao.qa.mock.service.core.kit.ErrorMsg;
import com.yuanniao.qa.mock.service.core.kit.StringKit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class HistroyWrapper extends BaseRuleManager implements RuleManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(HistroyWrapper.class);
    private Map<String, List<Context>> histroy = new HashMap<String, List<Context>>();
    private RuleManager ruleManager;

    public HistroyWrapper(RuleManager ruleManager) {
        this.ruleManager = ruleManager;
    }

    @Override
    public void dispatch(Context context) {
        String uri = context.getPath();
        try {
            if ("/internal/history/query".equals(uri)) {
                queryHistroy(context);
            } else if ("/internal/history/clear".equals(uri)) {
                clearHistroy(context);
            } else {
                this.ruleManager.dispatch(context);
                addHistroy(context);
            }
        } catch (Exception e) {
            handleError(context, e);
        } finally {
            common(context);
        }
    }

    private synchronized void addHistroy(Context context) {
        if(context.getPath().startsWith("/internal")) {
            return;
        }
        String id = context.getRuleId();
        if(StringKit.isEmpty(id)) {
            return;
        }
        if(histroy.containsKey(id)) {
            List<Context> ls = histroy.get(id);
            ls.add(context);
        } else {
            List<Context> ls = new LinkedList<Context>();
            ls.add(context);
            histroy.put(id, ls);
        }
    }

    private synchronized void clearHistroy(Context context) {
        histroy.remove(context.getRuleId());
        context.setBody("{\"status\":0}".getBytes());
    }

    private void queryHistroy(Context context) {
        String id = context.getRuleId();
        if(histroy.containsKey(id)) {
            JSONArray hsitroy = new JSONArray();
            List<Context> ls = histroy.get(id);
            int lsSize = ls.size();
            for (int i = 0; i < lsSize; i++) {
                JSONObject entry = new JSONObject();
                entry.put("header", new JSONObject(ls.get(i).getHeaderParam()));
                entry.put("body", new String(Base64.getEncoder().encode(ls.get(i).getBodyParam().getBytes())));
                hsitroy.add(entry);
            }
            context.setBody(String.format("{\"status\":0, \"hsitroy\":%s}", hsitroy.toJSONString()).getBytes());
        } else {
            throw ErrorMsg.RULE_NOT_FOUND;
        }
    }
}
