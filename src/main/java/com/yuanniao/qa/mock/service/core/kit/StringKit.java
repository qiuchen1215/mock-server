package com.yuanniao.qa.mock.service.core.kit;

public class StringKit {
    public static boolean isNotEmpty(String s) {
        return !isEmpty(s);
    }

    public static boolean isEmpty(String s) {
        return isNull(s) ? true : s.length() == 0 ? true : false;
    }

    public static boolean isNull(String s) {
        return s == null ? true : false;
    }

    public static String firstLetterToLowercase(String s) {
        if(isEmpty(s)) {
            return s;
        }
        return s.substring(0, 1).toLowerCase() + s.substring(1);
    }

    public static String splitCamelName(String s, String split) {
        if (isEmpty(s)) {
            return s;
        }
        StringBuilder buf = new StringBuilder(s.length() * 2);
        buf.append(Character.toLowerCase(s.charAt(0)));
        for (int i = 1; i < s.length(); i ++) {
            char c = s.charAt(i);
            if (c >= 'A' && c <= 'Z') {
                buf.append(split);
                buf.append(Character.toLowerCase(c));
            } else {
                buf.append(c);
            }
        }
        return buf.toString();
    }

    public static String[] splitByComma(String s) {
        if (isEmpty(s)) {
            return new String[0];
        }
        return s.split(",");
    }
}
