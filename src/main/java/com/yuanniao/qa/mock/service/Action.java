package com.yuanniao.qa.mock.service;

import com.yuanniao.qa.mock.service.core.Context;
import com.yuanniao.qa.mock.service.core.RuleManager;
import com.yuanniao.qa.mock.service.core.kit.BeanFactory;
import com.yuanniao.qa.mock.service.core.kit.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Properties;

@Controller
public class Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(Action.class);
    private static RuleManager ruleManager;

    static {
        Properties properties = new Properties();
        try {
            properties.load(Action.class.getResourceAsStream("/mock.properties"));
        } catch (Exception e) {
            properties.clear();
            properties.put("rule.manager", "com.closeli.qa.mock.service.core.spi.rulemanager.DefaultRuleManager");
            properties.put("rule.manager^", "com.closeli.qa.mock.service.core.spi.rulemanager.HistroyWrapper,com.closeli.qa.mock.service.core.spi.rulemanager.DnsWrapper");
            properties.put("parser.list", "com.closeli.qa.mock.service.core.spi.parser.JsonParser");
        }
        ruleManager = BeanFactory.createBean(RuleManager.class, properties);
    }

    @RequestMapping(value = "**")
    @ResponseBody
    public void mock(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Context context = Converter.toContext(request);
        ruleManager.dispatch(context);
        Converter.toResponse(context, response);
        LOGGER.info(context.toString());
    }
}
