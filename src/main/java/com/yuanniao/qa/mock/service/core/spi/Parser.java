package com.yuanniao.qa.mock.service.core.spi;


import com.yuanniao.qa.mock.service.core.Context;

public interface Parser {
    public boolean isMatch(String name);
    public void parse(Context context);
}
