package com.yuanniao.qa.mock.service.core;

import java.util.HashMap;
import java.util.Map;

public class Context {
    private String method;
    private String path;
    private String from;
    private Map<String, Object> headerParam;
    private String pathParam;
    private String bodyParam;
    private Rule rule;
    private int code;
    private String[] header;
    private byte[] body;
    private Map<String, String> other;

    public Context() {
        rule = new Rule();
        other = new HashMap<String, String>();
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Map<String, Object> getHeaderParam() {
        return headerParam;
    }

    public void setHeaderParam(Map<String, Object> headerParam) {
        this.headerParam = headerParam;
    }

    public String getPathParam() {
        return pathParam;
    }

    public void setPathParam(String pathParam) {
        this.pathParam = pathParam;
    }

    public String getBodyParam() {
        return bodyParam;
    }

    public void setBodyParam(String bodyParam) {
        this.bodyParam = bodyParam;
    }

    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }

    public String getRuleId() {
        return this.rule.getId();
    }

    public void setRuleId(String id) {
        this.rule.setId(id);
    }

    public String getRuleMethod() {
        return this.rule.getMethod();
    }

    public void setRuleMethod(String method) {
        this.rule.setMethod(method);
    }

    public String getRulePath() {
        return this.rule.getPath();
    }

    public void setRulePath(String path) {
        this.rule.setPath(path);
    }

    public String getRuleFrom() {
        return this.rule.getFrom();
    }

    public void setRuleFrom(String from) {
        this.rule.setFrom(from);
    }

    public String getRuleParser() {
        return this.rule.getParser();
    }

    public void setRuleParser(String parser) {
        this.rule.setParser(parser);
    }

    public String getRuleScript() {
        return this.rule.getScript();
    }

    public void setRuleScript(String script) {
        this.rule.setScript(script);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String[] getHeader() {
        return header == null ? new String[0] : header;
    }

    public void setHeader(String[] header) {
        this.header = header;
    }

    public byte[] getBody() {
        return body == null ? new byte[0] : body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }

    public void setOther(String key, String value) {
        other.put(key, value);
    }

    public String getOther(String key) {
        return other.get(key);
    }

    @Override
    public String toString() {
        StringBuffer dns = new StringBuffer();
        dns.append("[");
        for (String key : other.keySet()) {
            dns.append(key).append(":").append(other.get(key)).append(", ");
        }
        dns.append("]");
        StringBuffer sb = new StringBuffer();
        sb.append("from=").append(from).append(" ");
        sb.append("method=").append(method).append(" ");
        sb.append("path=").append(path).append(" ");
        sb.append("pathParam=").append(pathParam).append(" ");
        sb.append("bodyParam=").append(bodyParam).append(" ");
        sb.append("body=").append(new String(body)).append(" ");
        sb.append("other=").append(dns.toString()).append(" ");
        return sb.toString();
    }
}
