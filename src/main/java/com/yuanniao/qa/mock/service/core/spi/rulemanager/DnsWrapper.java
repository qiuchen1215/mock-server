package com.yuanniao.qa.mock.service.core.spi.rulemanager;

import com.yuanniao.qa.mock.service.core.Context;
import com.yuanniao.qa.mock.service.core.RuleManager;
import com.yuanniao.qa.mock.service.core.kit.ErrorMsg;
import com.yuanniao.qa.mock.service.core.kit.StringKit;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class DnsWrapper extends BaseRuleManager implements RuleManager {
    private Map<String, String> dnsMap = new HashMap<String, String>();
    private RuleManager ruleManager;

    public DnsWrapper(RuleManager ruleManager) {
        this.ruleManager = ruleManager;
    }

    @Override
    public void dispatch(Context context) {
        String uri = context.getPath();
        try {
            if ("/internal/dns/add".equals(uri)) {
                addDns(context);
            } else if ("/internal/dns/clear".equals(uri)) {
                clearDns(context);
            } else if ("/internal/dns/query".equals(uri)) {
                queryDns(context);
            } else {
                if ("/internal/rule/add".equals(uri)) {
                    String from = context.getRuleFrom();
                    if(dnsMap.containsKey(from)) {
                        context.setRuleFrom(dnsMap.get(from));
                    }
                }
                this.ruleManager.dispatch(context);
            }
        } catch (Exception e) {
            handleError(context, e);
        } finally {
            common(context);
        }
    }

    private synchronized void addDns(Context context) {
        String key = context.getOther("dns.key");
        String value = context.getOther("dns.value");
        if(StringKit.isEmpty(key)) {
            throw ErrorMsg.PARAM_FORMAT_ERROR;
        }
        if(StringKit.isEmpty(value)) {
            throw ErrorMsg.PARAM_FORMAT_ERROR;
        }
        dnsMap.put(key, value);
        context.setBody("{\"status\":0}".getBytes());
    }

    private void clearDns(Context context) {
        dnsMap.clear();
        context.setBody("{\"status\":0}".getBytes());
    }

    private void queryDns(Context context) {
        StringBuffer dns = new StringBuffer();
        dns.append("{");
        boolean flag = true;
        for (String key : dnsMap.keySet()) {
            if(flag) {
                flag = false;
            } else {
                dns.append(", ");
            }
            dns.append("\"").append(key).append("\"").append(":").append("\"").append(dnsMap.get(key)).append("\"");
        }
        dns.append("}");
        context.setBody(String.format("{\"status\":0, \"dns\":%s}", dns.toString()).getBytes());
    }
}
