package com.yuanniao.qa.mock.service.core;

public interface RuleManager {
    public void dispatch(Context context);
}
