package com.yuanniao.qa.mock.service.core.kit;


import com.yuanniao.qa.mock.service.core.Context;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class Converter {
    public static void toResponse(Context context, HttpServletResponse response) throws IOException {
        response.setStatus(context.getCode());
        String[] header = context.getHeader();
        if(header != null && header.length % 2 == 0) {
            for (int i = 0; i < header.length; i = i + 2) {
                response.setHeader(header[i], header[i + 1]);
            }
        }
        OutputStream stream = response.getOutputStream();
        stream.write(context.getBody());
        stream.flush();
//        stream.close();
    }

    public static Context toContext(HttpServletRequest request) throws IOException {
        Map<String, Object> headerParam = new HashMap<String, Object>();
        Enumeration<String> enumeration = request.getHeaderNames();
        while(enumeration.hasMoreElements()) {
            String name = enumeration.nextElement();
            headerParam.put(name, request.getHeader(name));
        }
        String path = request.getRequestURI();
        Context context = new Context();
        context.setHeaderParam(headerParam);
        context.setPath(request.getRequestURI());
        if(path.startsWith("/internal/dns/add")) {
            String key = request.getParameter("key");
            String value = request.getParameter("value");
            context.setOther("dns.key", key);
            context.setOther("dns.value", value);
         }else if(path.startsWith("/internal")) {
            setInternalParam(request, context);
        } else {
            StringBuffer bodyParam = new StringBuffer();
            if(request.getContentLength() < 1024) {
                Map<String, String[]> map = request.getParameterMap();
                boolean isFirst = true;
                for (String key : map.keySet()) {
                    if(isFirst) {
                        isFirst = false;
                    } else {
                        bodyParam.append("&");
                    }
                    bodyParam.append(key).append("=").append(map.get(key)[0]);
                }
            } else {
                bodyParam.append("body too long");
            }
//            context.setPathParam(request.getQueryString());
            context.setBodyParam(bodyParam.toString());
        }
        context.setPath(path);
        context.setMethod(request.getMethod());
        context.setFrom(request.getRemoteAddr());
        return context;
    }

    private static void setInternalParam(HttpServletRequest request, Context context) {
        String method = request.getParameter("method");
        if(StringKit.isNotEmpty(method)) {
            context.setRuleMethod(method);
        } else {
            context.setRuleMethod("");
        }
        String path = request.getParameter("path");
        if(StringKit.isNotEmpty(path)) {
            context.setRulePath(path);
        } else {
            context.setRulePath("");
        }
        String script = request.getParameter("script");
        if(StringKit.isNotEmpty(script)) {
            context.setRuleScript(script);
        } else {
            context.setRuleScript("");
        }
        String parser = request.getParameter("parser");
        if(StringKit.isNotEmpty(parser)) {
            context.setRuleParser(parser);
        } else {
            context.setRuleParser("json");
        }
        String form = request.getParameter("from");
        if(StringKit.isNotEmpty(form)) {
            context.setRuleFrom(form);
        } else {
            context.setRuleFrom(request.getRemoteAddr());
        }
        String id = request.getParameter("id");
        if(StringKit.isNotEmpty(id)) {
            context.setRuleId(id);
        } else {
            context.setRuleId("");
        }
    }
}
