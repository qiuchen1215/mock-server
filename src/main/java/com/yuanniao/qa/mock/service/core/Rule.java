package com.yuanniao.qa.mock.service.core;

public class Rule {
    private String id;
    private String method;
    private String path;
    private String from;
    private String parser;
    private String script;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getParser() {
        return parser;
    }

    public void setParser(String parser) {
        this.parser = parser;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    @Override
    public String toString() {
        return String.format("{\"id\":\"%s\", \"method\":\"%s\", \"path\":\"%s\", \"from\":\"%s\", \"parser\":\"%s\", \"script\":\"%s\"}",
                id, method, path, from, parser, script);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rule rule = (Rule) o;

        if (!method.equals(rule.method)) return false;
        if (!path.equals(rule.path)) return false;
        return from.equals(rule.from);
    }

    @Override
    public int hashCode() {
        int result = method.hashCode();
        result = 31 * result + path.hashCode();
        result = 31 * result + from.hashCode();
        return result;
    }
}
