package com.yuanniao.qa.mock.service.core.kit;

public class ErrorMsg extends RuntimeException {
    public static final ErrorMsg PARSER_NOT_SUPPORTED = new ErrorMsg(1000, "PARSER_NOT_SUPPORTED");
    public static final ErrorMsg RULE_NOT_FOUND = new ErrorMsg(1001, "RULE_NOT_FOUND");
    public static final ErrorMsg RULE_METHOD_EMPTY = new ErrorMsg(1002, "RULE_METHOD_EMPTY");
    public static final ErrorMsg RULE_PATH_EMPTY = new ErrorMsg(1003, "RULE_PATH_EMPTY");
    public static final ErrorMsg RULE_SCRIPT_EMPTY = new ErrorMsg(1004, "RULE_SCRIPT_EMPTY");
    public static final ErrorMsg PARSER_FORMAT_ERROR = new ErrorMsg(1005, "PARSER_FORMAT_ERROR");
    public static final ErrorMsg PARAM_FORMAT_ERROR = new ErrorMsg(1005, "PARAM_FORMAT_ERROR");

    private int status;
    private String msg;

    public ErrorMsg(int status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    @Override
    public String toString() {
        return String.format("{\"status\":%s, \"msg\":\"%s\"}", status, msg);
    }
}
