## 概述

基于Spring Boot实现的多用户公共HttpMock服务。
本身作为一个服务注册到Eureka上，提供的Http接口有：增删改查Mock规则，查看删除Http请求的历史记录。

## 内部接口

|Method|Path|Body|Response|
|----|----|----|----|
|POST|/internal/rule/add|method=xxx&path=xxx&parser=xxx&from=xxx&script=xxx|{"status":0,"id":"xxx"}
|POST|/internal/rule/remove|id=xxx|{"status":0}|
|POST|/internal/rule/query|id=xxx|{"status":0,"rule":{"id":"xxx", "method":"xxx", "path":"xxx", "from":"xxx", "parser":"xxx", "script":"xxx}}|
|POST|/internal/history/query|id=xxx|{"status":0, "hsitroy":["xxx"]}|
|POST|/internal/history/clear|id=xxx|{"status":0}|
|POST|/internal/dns/add|key=xxx&value=xxx|{"status":0}|
|POST|/internal/dns/clear||{"status":0}|
|POST|/internal/dns/query||{"status":0, "dns":{"xxx":"xxx"}}|


## 详解

### /internal/rule/add
新增mock规则。
parser为可选参数，不填默认为json。
script格式为 {"code":xxx,"header":{"xxx":"xxx"},"body":"xxx"}。
form填写的是实际mock访问者的ip地址。
method填写的是待访问接口的请求方法。
path填写的是待访问接口的请求路径（不含ip和端口）。

### /internal/rule/remove
根据id删除规则。
id是由add接口返回得到。
若id为空，则删除该from下的所有规则。

### /internal/rule/query
根据id查询规则。

### /internal/history/query
根据id查询该规则被调用的历史记录。
hsitroy字段的值是字符串数组，每个项表示的是每次记录中url或body中的参数，如：["type=xxx&token=xxx"]。

### /internal/history/clear
清除该id下的历史记录。

### /internal/dns/add
新增本地mock的dns缓存

### /internal/dns/clear
删除本地mock的所有dns缓存

### /internal/dns/query
查询本地mock的所有dns缓存